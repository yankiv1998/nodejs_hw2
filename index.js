require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const app = express();
const db = require('./db');
const {authRouter} = require('./src/authRouter');
const {notesRouter} = require('./src/notesRouter');
const {userRouter} = require('./src/userRouter');


const port = process.env.PORT || 8080;
app.use(express.json());
app.use(morgan('tiny'));

// app.use('/api/files', filesRouter);
app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
  try {
    await db.dbconnect();
    app.listen(port);
    console.log(`server listening on port ${port}`);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER
app.use(errorHandler);

// eslint-disable-next-line require-jsdoc
function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({'message': 'Server error'});
}
