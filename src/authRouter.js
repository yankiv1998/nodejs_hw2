const express = require('express');
// eslint-disable-next-line new-cap
const authRouter = express.Router();
const {User} = require('./models/userModel');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');


const generateAccessToken = (id, username) => {
  const payload ={
    id,
    username,
  };
  return jwt.sign(payload, process.env.SECRET_TOKEN, {expiresIn: '2h'});
};

authRouter.post('/register', async (req, res)=> {
  try {
    const {username, password} = req.body;
    const candidate = await User.findOne({username});
    if (candidate) {
      return res.status(400).json({message: 'User already exist'});
    }
    const hashPassword = bcrypt.hashSync(password, 6);
    const user = new User(
        {
          username,
          password: hashPassword,
          createdDate: new Date().toISOString(),
        },
    );
    await user.save();
    return res.json({message: 'User has been created'});
  } catch (e) {
    console.log(e);
    res.status(400).json({message: 'Registration error'});
  }
});

authRouter.post('/login', async (req, res)=>{
  try {
    const {username, password} = req.body;
    const user = await User.findOne({username});
    if (!user) {
      return res.status(400).json({message: `User not fond`});
    }
    const validPassword = bcrypt.compareSync(password, user.password);
    if (!validPassword) {
      return res.status(400).json({message: 'Password is not valid'});
    }
    const token = generateAccessToken(user._id, user.username);
    return res.json({jwt_token: token, message: 'Success'});
  } catch (e) {
    console.log(e);
    res.status(400).json({message: 'Login error'});
  }
});


module.exports = {
  authRouter,
};
