const {Schema, model} = require('mongoose');

const noteSchema = new Schema({
  message: {
    type: String,
    required: true,
  },
  completed: {
    type: Boolean,
    required: true,
  },
  userId: {
    type: String,
    required: true,
  },
  createdDate: {
    type: String,
    required: true,
  },

});

const Note = model('notes', noteSchema);

module.exports = {
  Note,
};
