const express = require('express');
// eslint-disable-next-line new-cap
const notesRouter = express.Router();

const {authMiddleware} = require('./authMiddleware');
const {Note} = require('./models/notesModel');

notesRouter.post('', authMiddleware, async (req, res)=>{
  try {
    const note = new Note({
      message: req.body.text,
      completed: false,
      userId: req.user._id,
      createdDate: new Date().toISOString(),
    });
    await note.save();
    res.json({message: 'Success'});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
});
notesRouter.get('/:id', authMiddleware, async (req, res)=>{
  try {
    const {id} = req.params;
    const note = await Note.findById(id);
    // if (note?.userId !== req.user._id) {
    //   note = null;
    // }

    res.json({note});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
});
notesRouter.put('/:id', authMiddleware, async (req, res)=>{
  try {
    const {id} = req.params;
    const note = await Note.findById(id);
    // if (note?.userId !== req.user._id) {
    //   note = null;
    // }
    if (!note) {
      return res.status(400).json({message: 'Note is not found'});
    }
    note.message = req.body.text;
    await note.save();

    res.json({message: 'Success'});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
});

notesRouter.patch('/:id', authMiddleware, async (req, res)=>{
  try {
    const {id} = req.params;
    const note = await Note.findById(id);
    // if (note?.userId !== req.user._id) {
    //   note = null;
    // }
    if (!note) {
      return res.status(400).json({message: 'Note is not found'});
    }
    note.completed = !note.completed;
    await note.save();

    res.json({message: 'Success'});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
});
notesRouter.delete('/:id', authMiddleware, async (req, res)=>{
  try {
    const {id} = req.params;
    const note = await Note.findById(id);

    if (!note) {
      return res.status(400).json({message: 'Note is not found'});
    }
    await note.delete();

    res.json({message: 'Success'});
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
});
notesRouter.get('', authMiddleware, async (req, res)=>{
  let {offset, limit} = req.query;

  offset = +offset ||0;
  limit = +limit || 10;

  try {
    const notes = await Note.find({userId: req.user._id}, null, {
      limit,
      skip: offset,
    });
    const count = await Note.find({userId: req.user._id}).count();

    return res.status(200).json({
      offset,
      limit,
      count,
      notes,
    });
  } catch (err) {
    return res.status(400).json({message: err.message});
  }
});

module.exports = {
  notesRouter,
};
