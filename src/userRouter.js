const express = require('express');
// eslint-disable-next-line new-cap
const userRouter = express.Router();
const {User} = require('./models/userModel');
const bcrypt = require('bcryptjs');
const authMiddleware = require('./authMiddleware');


userRouter.get('/me', authMiddleware.authMiddleware, async (req, res)=>{
  return res.json({user: req.user});
});

userRouter.patch('/me', authMiddleware.authMiddleware, async (req, res)=>{
  try {
    const {oldPassword, newPassword} = req.body;
    const user = await User.findById(req.user._id);
    if (!user) {
      return res.status(400).json({message: `User not fond`});
    }
    const validPassword = bcrypt.compareSync(oldPassword, user.password);
    if (!validPassword) {
      return res.status(400).json({message: 'Password is not valid'});
    }
    user.password = bcrypt.hashSync(newPassword, 6);
    await user.save();
    return res.json({message: 'Password has been changed'});
  } catch (e) {
    console.log(e);
    res.status(400).json({message: 'Login error'});
  }
});
userRouter.delete('/me', authMiddleware.authMiddleware, async (req, res)=> {
  try {
    await User.findByIdAndDelete(req.user._id);
    res.json({message: 'User has been deleted'});
  } catch (e) {
    console.log(e);
    res.status(400).json({message: 'Login error'});
  }
});

module.exports = {
  userRouter,
};
